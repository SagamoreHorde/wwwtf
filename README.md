<!DOCTYPE html>
<html>
	<head>
		<link href="styles1.css" rel="stylesheet" type="text/css">
		<style>
            em { display: none; }
        </style>
		<title>Night Lovell</title>

	</head>
	<body>
		<div class="header"><h1>Night Lovell</h1></div>
        <ul class="main-menu">
            <li><a href="#company">О сайте</a></li>
            <li>
                <a href="#services">Услуги</a>
                <ul class="sub-menu">
                    <li><a href="#1">Битмэйк</a></li>
                    <li><a href="#2">Продвижение</a></li>
                    <li><a href="#3">Контекст</a></li>
                </ul>
            </li>
            <li><a href="#team">Сотрудники</a></li>
        </ul>
		<table>
                <tr>
                    <th>Песни исполнителя</th><br>
                </tr>
                <tr>
                	<td>Dark Light <em>Album-Concept Vague  2014 г.</em> </td>
                </tr>
                <tr>
                	<td>Deira City Centre <em>Album-Concept Vague  2014 г.</em ></td>
                </tr>
                <tr>
                	<td>Concept Nothing <em>Album-Concept Vague  2014 г.</em> </td>
                </tr>
                <tr>
                	<td>Contraband <em>Album-Red Teenage Melody 2016 г.</em> </td>
                </tr>
                <tr>
                	<td><a href="https://www.google.com.ua/search?biw=1366&bih=662&q=night+lovell+%D0%BF%D0%B5%D1%81%D0%BD%D0%B8&stick=H4sIAAAAAAAAAONgFuLVT9c3NEwyjy_KNTUpUULlaollJ1vp55YWZybrJxaVZBaXWBXn56UXAwBJoX1xOAAAAA&sa=X&ved=0ahUKEwjw0ryt5__YAhXCbFAKHQ2mACUQMQiuATAW">More (25+ songs)</a></td>
                </tr>
            </table>
            <h2>Биография</h2>
        <p class="info">Всё началось осенью 2014 года, когда молодой парень из Канады,<br> 
       					ранее известный в молодых кругах, как битмейкер "KLNV",выпустил <br>
                        свой первыйи пока единственный полноценный альбом, под названием<br>
                        "Concept Vague".Кто это и что это за звук?Всё это остаётся <br>
                        загадкой до сих пор.Night Lovell- это образ неразгаданной тайны.<br>
                        О нём не пишут в знаменитыхизданиях, у него нету контрактов<br>
                        с крупными лейблами,нету миллионов просмотров на канале VEVO,<br>
                        но есть то, чего хватает для того, чтобы рвать душу всем и<br>
                        каждому, кто прослушает его композиции- свой уникальный,<br>
                        неповторимый,холодный звук.Трэк за трэком, лёгким касанием <br>
                        снежной руки, Lovell рвёт,метает, крушит и убивает наше <br>
                        сознание при помощи своей ледяной подачи.Дословно, в переводе<br>
                        с английского, "Concept Vague"- это "расплывчатая,<br>
						неопределённая концепция", и это название, как никто другое<br>
                        подходит к альбому. Начиная с обложки и заканчивая продакшном, <br>
                        в каждой мелочи этой работы таится загадка. Текста Ловелла <br>
                        наполнены метафорами о природе, холоде,севере и повседневной <br>
                        жизни. Это то, что мелькает в голове, лишь мысли и <br>
                        недоконченные предложения, которые в комбинации <br>
                        с мелодичными проигрышами создали свежее ответвление клауд-рэпа.<br>
                        В своих текстах, артист часто выражает некую злость,<br>
                        которая больше походит на холодную апатию, нежели агрессивную<br>
						ненависть. Советуем вам прослушать композиции в последовательности, <br>
						представленной нами, так вы сможете целостно и полно образно<br>
						понять настроение автора.<br>
		</p>
            <img src="baby.jpg" width="300" height="300" alt="NightLovell"  title="NightLovell">
            <p class="bio"> <strong>Night Lovell</strong><br>
           <strong>Рэпер</strong><br>
            <strong>Родился:</strong> 29 мая 1997 г.  <a href="https://www.google.com.ua/search?biw=1366&bih=662&q=%D0%9E%D1%82%D1%82%D0%B0%D0%B2%D0%B0&stick=H4sIAAAAAAAAAOPgE-LVT9c3NEwyjy_KNTUpUeLQz9U3MM0uztASy0620i9IzS_ISQVSRcX5eVZJ-UV5AHSmtzMzAAAA&sa=X&ved=0ahUKEwjf3auJ7v_YAhWPZFAKHXFbBUwQmxMImwEoATAT">Оттава, Канада</a><br>
            <strong>Полное имя:</strong> Shermar Paul<br>
            <strong>Жанр:</strong> Хип-хоп/рэп</p>

            <div class="footer">Информация взята у <a href="https://www.last.fm">сайта</a></div>
	</body>
</html>